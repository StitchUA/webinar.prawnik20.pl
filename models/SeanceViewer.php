<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%seance_viewers}}".
 *
 * @property int $id
 * @property string $registration_id Rejestracja widza na seans
 * @property string $viewer_token Token widza do oglądania seansu
 * @property string $entranced_at Data i czas wejścia widza na oglądanie seansu
 * @property string $ip IP adres widza
 * @property string $user_agent USER_AGENT widza
 * @property string $status Status widza: ACTIVE, BLOCKED
 *
 * @property SeanceRegistration $registration
 */
class SeanceViewer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%seance_viewers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registration_id', 'viewer_token', 'entranced_at'], 'required'],
            [['registration_id', 'status'], 'integer'],
            [['entranced_at'], 'safe'],
            [['viewer_token'], 'string', 'max' => 80],
            [['ip'], 'string', 'max' => 40],
            [['user_agent'], 'string', 'max' => 255],
            [['registration_id'], 'exist', 'skipOnError' => true, 'targetClass' => SeanceRegistration::class, 'targetAttribute' => ['registration_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'registration_id' => Yii::t('app', 'Rejestracja widza na seans'),
            'viewer_token' => Yii::t('app', 'Token widza do oglądania seansu'),
            'entranced_at' => Yii::t('app', 'Data i czas wejścia widza na oglądanie seansu'),
            'ip' => Yii::t('app', 'IP adres widza'),
            'user_agent' => Yii::t('app', 'USER_AGENT widza'),
            'status' => Yii::t('app', 'Status widza: ACTIVE, BLOCKED'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistration()
    {
        return $this->hasOne(SeanceRegistration::class, ['id' => 'registration_id']);
    }
}
