<?php

use yii\db\Migration;

/**
 * Class m191013_174136_add_column_film_id_to_file_sources_table
 */
class m191013_174136_add_column_film_id_to_file_sources_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%file_sources}}', 'film_id', $this->integer(11)->unsigned()->notNull());
        $this->addCommentOnColumn('{{%file_sources}}', 'film_id', 'ID filmu do którego należy źródło');
        $this->createIndex(
            'idx-film_sources',
            '{{%file_sources}}',
            'film_id'
        );
        $this->addForeignKey(
            'fk-film_sources',
            '{{%file_sources}}',
            'film_id',
            '{{%films}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-film_sources', '{{%file_sources}}');
        $this->dropIndex('idx-film_sources', '{{%file_sources}}');
        $this->dropColumn('{{%file_sources}}', 'film_id');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191013_174136_add_column_film_id_to_file_sources_table cannot be reverted.\n";

        return false;
    }
    */
}
