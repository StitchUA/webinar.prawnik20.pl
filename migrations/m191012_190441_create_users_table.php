<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m191012_190441_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey()->unsigned(),
            'first_name' => $this->string(65)->notNull(),
            'last_name' => $this->string(250)->null(),
            'email' => $this->string(255)->notNull(),
            'registred_at' => $this->dateTime()->notNull(),
            'crm_webuser_id' => $this->integer(11)->null(),
            'crm_webuser_promo_code' => $this->string(16)->null(),
            'crm_webuser_promo_link' => $this->string(255)->null(),
            'crm_refferer_promo_code' => $this->string(16)->null(),
        ]);

        $this->addCommentOnColumn('{{%users}}', 'first_name', 'Imię użytkownika');
        $this->addCommentOnColumn('{{%users}}', 'last_name', 'Nazwisko użytkownika');
        $this->addCommentOnColumn('{{%users}}', 'email', 'Email użytkownika');
        $this->addCommentOnColumn('{{%users}}', 'registred_at', 'Data rejestracji użytkownika');
        $this->addCommentOnColumn(
            '{{%users}}',
            'crm_webuser_id',
            'ID konta \'rejestrowany  na webinar\' z CRM\'u');

        $this->addCommentOnColumn(
            '{{%users}}',
            'crm_webuser_promo_code',
            'Promo kod konta \'rejestrowany  na webinar\' z CRM\'u');
        $this->addCommentOnColumn(
            '{{%users}}',
            'crm_webuser_promo_link',
            'Link do promowania webinaru konta \'rejestrowany  na webinar\' z CRM\'u');
        $this->addCommentOnColumn(
            '{{%users}}',
            'crm_refferer_promo_code',
            'Promo kod osoby polecającej z CRM\'u');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
