<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%file_type_extensions}}`.
 */
class m191013_062718_create_file_type_extensions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%file_type_extensions}}', [
            'id' => $this->primaryKey()->unsigned(),
            'type_id' => $this->integer(11)->unsigned()->notNull(),
            'name' => $this->string(12)->notNull()
        ]);

        $this->createIndex(
            'idx-file_type_exts',
            '{{%file_type_extensions}}',
            'type_id'
        );

        $this->addForeignKey(
            'fk-file_type_exts',
            '{{%file_type_extensions}}',
            'type_id',
            '{{%file_types}}',
            'id',
            'CASCADE'
        );

        $this->addCommentOnColumn('{{%file_type_extensions}}',
            'type_id', 'ID typu pliku');
        $this->addCommentOnColumn('{{%file_type_extensions}}',
            'name', 'Rozszerzenie');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-file_type_exts', '{{%file_type_extensions}}');
        $this->dropIndex('idx-file_type_exts', '{{%file_type_extensions}}');
        $this->dropTable('{{%file_type_extensions}}');
    }
}
