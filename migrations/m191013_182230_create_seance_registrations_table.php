<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%seance_registrations}}`.
 */
class m191013_182230_create_seance_registrations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%seance_registrations}}', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer(11)->notNull()->unsigned(),
            'seance_id' => $this->integer(11)->unsigned()->notNull(),
            'token' => $this->string(80)->unique()->notNull(),
            'registred_at' => $this->dateTime()->notNull()
        ]);

        $this->addCommentOnColumn('{{%seance_registrations}}', 'user_id', 'Który użytkownik się zarejestrował na obejrzanie seansu');
        $this->addCommentOnColumn('{{%seance_registrations}}', 'seance_id', 'Seans do obejrzania');
        $this->addCommentOnColumn('{{%seance_registrations}}', 'token', 'Unikalnu klucz do oglądania');
        $this->addCommentOnColumn('{{%seance_registrations}}', 'registred_at', 'Data i czas rejestracji');

        $this->createIndex(
            'idx-registred_user',
            '{{%seance_registrations}}',
            'user_id'
        );
        $this->addForeignKey(
            'fk-registred_user',
            '{{%seance_registrations}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-registred_seance',
            '{{%seance_registrations}}',
            'seance_id'
        );
        $this->addForeignKey(
            'fk-registred_seance',
            '{{%seance_registrations}}',
            'seance_id',
            '{{%seances}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-registred_user', '{{%seance_registrations}}');
        $this->dropForeignKey('fk-registred_seance', '{{%seance_registrations}}');

        $this->dropIndex('idx-registred_user', '{{%seance_registrations}}');
        $this->dropIndex('idx-registred_seance', '{{%seance_registrations}}');
        
        $this->dropTable('{{%seance_registrations}}');
    }
}
