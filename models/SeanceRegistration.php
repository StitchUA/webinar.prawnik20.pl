<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%seance_registrations}}".
 *
 * @property string $id
 * @property string $user_id Który użytkownik się zarejestrował na obejrzanie seansu
 * @property string $seance_id Seans do obejrzania
 * @property string $token Unikalnu klucz do oglądania
 * @property string $registred_at Data i czas rejestracji
 *
 * @property Seance $seance
 * @property User $user
 * @property SeanceViewer[] $seanceViewers
 */
class SeanceRegistration extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%seance_registrations}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'seance_id', 'token', 'registred_at'], 'required'],
            [['user_id', 'seance_id'], 'integer'],
            [['registred_at'], 'safe'],
            [['token'], 'string', 'max' => 80],
            [['token'], 'unique'],
            [['seance_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seance::class, 'targetAttribute' => ['seance_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Który użytkownik się zarejestrował na obejrzanie seansu'),
            'seance_id' => Yii::t('app', 'Seans do obejrzania'),
            'token' => Yii::t('app', 'Unikalnu klucz do oglądania'),
            'registred_at' => Yii::t('app', 'Data i czas rejestracji'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeance()
    {
        return $this->hasOne(Seance::class, ['id' => 'seance_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeanceViewers()
    {
        return $this->hasMany(SeanceViewer::class, ['registration_id' => 'id']);
    }
}
