<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%file_sources}}".
 *
 * @property string $id
 * @property string $file_path Scieżka do pliku
 * @property string $type_id ID typu pliku
 * @property string $film_id ID filmu do którego należy źródło
 *
 * @property FileType $type
 * @property Film $film
 */
class FileSource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%file_sources}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'film_id'], 'required'],
            [['type_id', 'film_id'], 'integer'],
            [['file_path'], 'string', 'max' => 1024],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileType::class, 'targetAttribute' => ['type_id' => 'id']],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Film::class, 'targetAttribute' => ['film_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'file_path' => Yii::t('app', 'Scieżka do pliku'),
            'type_id' => Yii::t('app', 'ID typu pliku'),
            'film_id' => Yii::t('app', 'ID filmu do którego należy źródło'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(FileType::class, ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Film::class, ['id' => 'film_id']);
    }
}
