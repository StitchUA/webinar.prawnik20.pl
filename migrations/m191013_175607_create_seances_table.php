<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%seances}}`.
 */
class m191013_175607_create_seances_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%seances}}', [
            'id' => $this->primaryKey()->unsigned(),
            'film_id' => $this->integer(11)->unsigned()->notNull(),
            'started_at' => $this->dateTime()->notNull(),
            'status' => $this->integer(1)->notNull()->defaultValue(0),
            'creared_by' => $this->integer(11)->unsigned()->notNull(),
            'creared_at' => $this->dateTime()->notNull()
        ]);

        $this->addCommentOnColumn('{{%seances}}', 'film_id', 'Film zapłanowany do obejrzenia');
        $this->addCommentOnColumn('{{%seances}}', 'started_at', 'Data i czas początku seansu');
        $this->addCommentOnColumn('{{%seances}}', 'status', 'Status seansu: WAITING_FOR_PLAING, PLAING, FINISHED');
        $this->addCommentOnColumn('{{%seances}}', 'creared_by', 'Kim stworzono seans');
        $this->addCommentOnColumn('{{%seances}}', 'creared_at', 'Data i czas tworzenia seansu');

        $this->createIndex(
            'idx-played_film',
            '{{%seances}}',
            'film_id'
        );
        $this->addForeignKey(
            'fk-played_film',
            '{{%seances}}',
            'film_id',
            '{{%films}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-seance_creator',
            '{{%seances}}',
            'creared_by'
        );
        $this->addForeignKey(
            'fk-seance_creator',
            '{{%seances}}',
            'creared_by',
            '{{%users}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-seance_creator', '{{%seances}}');
        $this->dropForeignKey('fk-played_film', '{{%seances}}');

        $this->dropIndex('idx-seance_creator', '{{%seances}}');
        $this->dropIndex('idx-played_film', '{{%seances}}');

        $this->dropTable('{{%seances}}');
    }
}
