<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%file_sources}}`.
 */
class m191013_063144_create_file_sources_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%file_sources}}', [
            'id' => $this->primaryKey()->unsigned(),
            'file_path' => $this->string(1024),
            'type_id' => $this->integer(11)->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'idx-file_type',
            '{{%file_sources}}',
            'type_id'
        );

        $this->addForeignKey(
            'fk-file_source_file_type',
            '{{%file_sources}}',
            'type_id',
            '{{%file_types}}',
            'id'
        );

        $this->addCommentOnColumn('{{%file_sources}}', 'file_path', 'Scieżka do pliku');
        $this->addCommentOnColumn('{{%file_sources}}', 'type_id', 'ID typu pliku');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-file_source_file_type', '{{%file_sources}}');
        $this->dropIndex('idx-file_type', '{{%file_sources}}');
        $this->dropTable('{{%file_sources}}');
    }
}
