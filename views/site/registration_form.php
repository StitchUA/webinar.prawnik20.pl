<form role="form" method="post" action="registration" name="registerForm"
      onsubmit="return process()" target="new">
    <input name="_token" type="hidden" value="2S5uHQIWU4MjjfXf4pEudbTfW73mQwUbUDNteUHn">
    <input type="hidden" name="webicode" value="7e3994814f">
    <input type="hidden" name="memberid" value="169105139238455355">
    <input type="hidden" name="page" value="registration">
    <input type="hidden" name="page_tag" value="formregistration">
    <input type="hidden" name="select_country">
    <input type="hidden" name="select_countryname">
    <input type="hidden" name="userid" value="0">
    <input type="hidden" name="selected_date" value="2019-10-13">
    <input type="hidden" name="selected_schedule" value="0">

    <input type="hidden" name="to_timezone" value="Europe/Berlin">
    <input type="hidden" name="from_timezone" value="Europe/Berlin">
    <input type="hidden" name="tz_selected" value="1">

    <!-- WEBINAR DATES -->
    <div class="schedule_block clearfix">
        <div class="form-group js_form_group_date">
            <div class="btn-group bootstrap-select">
                <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown"
                        role="button" data-id="dateSelect" title="Niedziela, 13 Październik 2019"
                        aria-expanded="false"><span class="filter-option pull-left"><i
                                class="glyphicon gd-ico-date"></i> Niedziela, 13 Październik 2019</span>&nbsp;<span
                            class="bs-caret"><span class="caret"></span></span></button>
                <div class="dropdown-menu open" role="combobox">
                    <ul class="dropdown-menu inner" role="listbox" aria-expanded="false">
                        <li data-original-index="0" class="disabled"><a tabindex="-1" class=""
                                                                        data-tokens="null"
                                                                        role="option" href="#"
                                                                        aria-disabled="true"
                                                                        aria-selected="false"><span><span
                                            class="glyphicon gd-ico-date"></span> </span><span
                                        class="text">Wybierz datę</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="1" class="selected"><a tabindex="0" class=""
                                                                        data-tokens="null"
                                                                        role="option"
                                                                        aria-disabled="false"
                                                                        aria-selected="true"><span
                                        class="glyphicon gd-ico-date"></span> <span class="text">Niedziela, 13 Październik 2019</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="2"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-date"></span> <span class="text">Poniedziałek, 14 Październik 2019</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                    </ul>
                </div>
                <select id="dateSelect" class="selectpicker" data-dropup-auto="false" data-size="4"
                        tabindex="-98">
                    <option value="" selected="" disabled="" data-icon="gd-ico-date">Wybierz datę
                    </option>

                    <option value="2019-10-13" data-icon="gd-ico-date">Niedziela, 13 Październik
                        2019
                    </option>

                    <option value="2019-10-14" data-icon="gd-ico-date">Poniedziałek, 14 Październik
                        2019
                    </option>
                </select></div>
        </div>

        <div class="form-group no_margin js_form_group_session">
            <div class="btn-group bootstrap-select">
                <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown"
                        role="button" data-id="timeSelect"
                        title="07:00 pm, Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna, GMT +2"
                        aria-expanded="false"><span class="filter-option pull-left"><i
                                class="glyphicon gd-ico-clock"></i> 07:00 pm, Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna, GMT +2</span>&nbsp;<span
                            class="bs-caret"><span class="caret"></span></span></button>
                <div class="dropdown-menu open" role="combobox">
                    <ul class="dropdown-menu inner" role="listbox" aria-expanded="false">
                        <li data-original-index="0" class="disabled"><a tabindex="-1" class=""
                                                                        data-tokens="null"
                                                                        role="option" href="#"
                                                                        aria-disabled="true"
                                                                        aria-selected="false"><span><span
                                            class="glyphicon gd-ico-clock"></span> </span><span
                                        class="text">Wybierz sesję...</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="1" class="selected"><a tabindex="0" class=""
                                                                        data-tokens="null"
                                                                        role="option"
                                                                        aria-disabled="false"
                                                                        aria-selected="true"><span
                                        class="glyphicon gd-ico-clock"></span> <span class="text">07:00 pm, Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                    </ul>
                </div>
                <select id="timeSelect" class="selectpicker" data-dropup-auto="false" data-size="4"
                        tabindex="-98">
                    <option value="" selected="" disabled="" data-icon="gd-ico-clock">Wybierz
                        sesję...
                    </option>

                    <option value="07:00 pm" data-icon="gd-ico-clock" data-schedule="0">07:00 pm,
                        Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna, GMT +2
                    </option>
                </select></div>
        </div>


        <div class="form-group no_margin js_form_group_timezone" style="display: none;">
            <div class="btn-group bootstrap-select">
                <button type="button" class="btn dropdown-toggle bs-placeholder btn-default"
                        data-toggle="dropdown" role="button" data-id="timezoneSelect"
                        title="Wybierz strefę czasową..."><span class="filter-option pull-left"><i
                                class="glyphicon gd-ico-global"></i> Wybierz strefę czasową...</span>&nbsp;<span
                            class="bs-caret"><span class="caret"></span></span></button>
                <div class="dropdown-menu open" role="combobox"
                     style="max-height: 180px; overflow: hidden;">
                    <ul class="dropdown-menu inner" role="listbox" aria-expanded="false"
                        style="max-height: 168px; overflow-y: auto;">
                        <li data-original-index="0" class="disabled selected"><a tabindex="-1"
                                                                                 class=""
                                                                                 data-tokens="null"
                                                                                 role="option"
                                                                                 href="#"
                                                                                 aria-disabled="true"
                                                                                 aria-selected="true"><span><span
                                            class="glyphicon gd-ico-global"></span> </span><span
                                        class="text">Wybierz strefę czasową...</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="1"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Pacific Time (US and Canada), GMT -7</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="2"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Mountain Time (US and Canada), GMT -6</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="3"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Central Time (US and Canada), GMT -5</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="4"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Eastern Time (US and Canada), GMT -4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="5"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Arizona, GMT -7</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="6"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Cook Islands, Hawaii, French Polynesia, GMT -10</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="7"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Alaska, Anchorage, GMT -8</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="8"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">London, GMT +1</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="9"><a tabindex="0" class="" data-tokens="null"
                                                       role="option" aria-disabled="false"
                                                       aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Dublin, Edinburgh, Lisbon, Casablanca, Morocco, GMT +1</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="10"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="11"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Belgrade, Bratislava, Budapest, Ljubljana, Prague, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="12"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Brussels, Copenhagen, Madrid, Paris, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="13"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Oslo, Sarajevo, Skopje, Warsaw, Zagreb, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="14"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Kaliningrad, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="15"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Athens, Bucharest, Helsinki, Kiev, GMT +3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="16"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Nicosia, Riga, Sofia, Tallinn, Vilnius, GMT +3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="17"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Moscow, St Petersburg, GMT +3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="18"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Melbourne, Sydney, Currie, Canberra, Hobart, GMT +11</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="19"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Adelaide, GMT +10:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="20"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Brisbane, GMT +10</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="21"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Queensland, GMT +10</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="22"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Darwin, GMT +9:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="23"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Eucla, GMT +8:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="24"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Perth, GMT +8</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="25"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Auckland, Wellington, GMT +13</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="26"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Rio de Janeiro, Sao Paulo, GMT -3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="27"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Pernambuco, Fernando de Noronha, Mid-Atlantic, GMT -2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="28"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Campo Grande, Mato Groso, GMT -4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="29"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Buenos Aires, GMT -3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="30"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Amapa, Montevideo, Rio Grande do Norte, Santiago, GMT -3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="31"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Antigua, Aruba, Georgetown, La Paz, San Juan, GMT -4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="32"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Boa Vista, Tefe, Porto Velho, GMT -4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="33"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Bermuda, Halifax, New Brunswick, Paraguay, GMT -3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="34"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Atlantic Time (Canada), GMT -3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="35"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Caracas, GMT -4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="36"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Rio Branco, Eirunepe, GMT -5</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="37"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Bogota, Jamaica, Lima, Panama, Quito, GMT -5</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="38"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Mexico City, GMT -5</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="39"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Belize, Costa Rica, El Salvadore, Guatemala, GMT -6</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="40"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Newfoundland, GMT -2:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="41"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Chersky, Severo-Kurilsk, GMT +11</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="42"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Deputatsky, Khabarovsk, Oymyakom, Vladivostok, GMT +10</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="43"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Guam, Magadan, Solomon Islands, New Caledonia, GMT +11</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="44"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Olenyok, Vilyuysk, Yakutsk, GMT +9</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="45"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Seoul, Osaka, Tokyo, GMT +9</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="46"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Pyongyang, GMT +8:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="47"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Taipei, Kuala Lumpur, Singapore, GMT +8</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="48"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Bratsk, Chita, Irkutsk, Ulaan Bataar, GMT +8</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="49"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Beijing, Chongqing, Hong Kong, Urumqi, GMT +8</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="50"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Krasnoyarsk, Dudinka, Igarka, Tura, GMT +7</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="51"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Bangkok, Hanoi, Jakarta, GMT +7</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="52"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Rangoon, GMT +6:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="53"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Astana, Dhaka, Novosibirsk, GMT +6</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="54"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Kathmandu, GMT +5:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="55"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Calcutta, Chennai, Mumbai, New Delhi, GMT +5:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="56"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Islamabad, Tashkent, Yekaterinburg, GMT +5</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="57"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Kabul, GMT +4:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="58"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Baku, GMT +4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="59"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Samara, Tbilisi, Yerevan, GMT +4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="60"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Abu Dhabi, Muscat, GMT +4</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="61"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Tehran, GMT +3:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="62"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Baghdad, Kuwait, Riyadh, GMT +3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="63"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Istanbul, Jerusalem, Minsk, Tel Aviv, GMT +3</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="64"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Capetown, Cairo, Harare, Johannesburg, Pretoria, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="65"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Kigali, Lusaka, Maputo, Tripoli, GMT +2</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="66"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Ndjamena, Kinshasa, West Central Africa, GMT +1</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="67"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Ghana, Monrovia, GMT +0</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="68"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Praia, Cabo Verde, GMT -1</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="69"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Azores, GMT +0</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="70"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Taiohae, Marquesas Islands, GMT -9:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="71"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Midway, Necker Island, Pago Pago, GMT -11</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="72"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Kamchatka, Bilbino, Pevek, Marshall Islands, GMT +12</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="73"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Fiji, GMT +12</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="74"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span class="text">Chatham Islands, GMT +13:30</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="75"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Nukualofa, GMT +13</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="76"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Apia, GMT +14</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="77"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">Kiritimati, GMT +14</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                        <li data-original-index="78"><a tabindex="0" class="" data-tokens="null"
                                                        role="option" aria-disabled="false"
                                                        aria-selected="false"><span
                                        class="glyphicon gd-ico-global"></span> <span
                                        class="text">UTC, GMT +0</span><span
                                        class="glyphicon glyphicon-ok check-mark"></span></a></li>
                    </ul>
                </div>
                <select id="timezoneSelect" class="selectpicker" data-dropup-auto="false"
                        data-size="4" data-for="" tabindex="-98">
                    <option value="" selected="" disabled="" data-icon="gd-ico-global">Wybierz
                        strefę czasową...
                    </option>
                    <option value="America/Los_Angeles" data-icon="gd-ico-global">Pacific Time (US
                        and Canada), GMT -7
                    </option>
                    <option value="America/Denver" data-icon="gd-ico-global">Mountain Time (US and
                        Canada), GMT -6
                    </option>
                    <option value="America/Chicago" data-icon="gd-ico-global">Central Time (US and
                        Canada), GMT -5
                    </option>
                    <option value="America/New_York" data-icon="gd-ico-global">Eastern Time (US and
                        Canada), GMT -4
                    </option>
                    <option value="America/Phoenix" data-icon="gd-ico-global">Arizona, GMT -7
                    </option>
                    <option value="Pacific/Honolulu" data-icon="gd-ico-global">Cook Islands, Hawaii,
                        French Polynesia, GMT -10
                    </option>
                    <option value="America/Anchorage" data-icon="gd-ico-global">Alaska, Anchorage,
                        GMT -8
                    </option>
                    <option value="Europe/London" data-icon="gd-ico-global">London, GMT +1</option>
                    <option value="Europe/Lisbon" data-icon="gd-ico-global">Dublin, Edinburgh,
                        Lisbon, Casablanca, Morocco, GMT +1
                    </option>
                    <option value="Europe/Berlin" data-icon="gd-ico-global">Amsterdam, Berlin, Bern,
                        Rome, Stockholm, Vienna, GMT +2
                    </option>
                    <option value="Europe/Budapest" data-icon="gd-ico-global">Belgrade, Bratislava,
                        Budapest, Ljubljana, Prague, GMT +2
                    </option>
                    <option value="Europe/Paris" data-icon="gd-ico-global">Brussels, Copenhagen,
                        Madrid, Paris, GMT +2
                    </option>
                    <option value="Europe/Sarajevo" data-icon="gd-ico-global">Oslo, Sarajevo,
                        Skopje, Warsaw, Zagreb, GMT +2
                    </option>
                    <option value="Europe/Kaliningrad" data-icon="gd-ico-global">Kaliningrad, GMT
                        +2
                    </option>
                    <option value="Europe/Bucharest" data-icon="gd-ico-global">Athens, Bucharest,
                        Helsinki, Kiev, GMT +3
                    </option>
                    <option value="Europe/Sofia" data-icon="gd-ico-global">Nicosia, Riga, Sofia,
                        Tallinn, Vilnius, GMT +3
                    </option>
                    <option value="Europe/Moscow" data-icon="gd-ico-global">Moscow, St Petersburg,
                        GMT +3
                    </option>
                    <option value="Australia/Sydney" data-icon="gd-ico-global">Melbourne, Sydney,
                        Currie, Canberra, Hobart, GMT +11
                    </option>
                    <option value="Australia/Adelaide" data-icon="gd-ico-global">Adelaide, GMT
                        +10:30
                    </option>
                    <option value="Australia/Brisbane" data-icon="gd-ico-global">Brisbane, GMT +10
                    </option>
                    <option value="Australia/Queensland" data-icon="gd-ico-global">Queensland, GMT
                        +10
                    </option>
                    <option value="Australia/Darwin" data-icon="gd-ico-global">Darwin, GMT +9:30
                    </option>
                    <option value="Australia/Eucla" data-icon="gd-ico-global">Eucla, GMT +8:30
                    </option>
                    <option value="Australia/Perth" data-icon="gd-ico-global">Perth, GMT +8</option>
                    <option value="Pacific/Auckland" data-icon="gd-ico-global">Auckland, Wellington,
                        GMT +13
                    </option>
                    <option value="America/Sao_Paulo" data-icon="gd-ico-global">Rio de Janeiro, Sao
                        Paulo, GMT -3
                    </option>
                    <option value="America/Noronha" data-icon="gd-ico-global">Pernambuco, Fernando
                        de Noronha, Mid-Atlantic, GMT -2
                    </option>
                    <option value="America/Campo_Grande" data-icon="gd-ico-global">Campo Grande,
                        Mato Groso, GMT -4
                    </option>
                    <option value="America/Argentina/Buenos_Aires" data-icon="gd-ico-global">Buenos
                        Aires, GMT -3
                    </option>
                    <option value="America/Santiago" data-icon="gd-ico-global">Amapa, Montevideo,
                        Rio Grande do Norte, Santiago, GMT -3
                    </option>
                    <option value="America/La_Paz" data-icon="gd-ico-global">Antigua, Aruba,
                        Georgetown, La Paz, San Juan, GMT -4
                    </option>
                    <option value="America/Boa_Vista" data-icon="gd-ico-global">Boa Vista, Tefe,
                        Porto Velho, GMT -4
                    </option>
                    <option value="America/Halifax" data-icon="gd-ico-global">Bermuda, Halifax, New
                        Brunswick, Paraguay, GMT -3
                    </option>
                    <option value="Canada/Atlantic" data-icon="gd-ico-global">Atlantic Time
                        (Canada), GMT -3
                    </option>
                    <option value="America/Caracas" data-icon="gd-ico-global">Caracas, GMT -4
                    </option>
                    <option value="America/Eirunepe" data-icon="gd-ico-global">Rio Branco, Eirunepe,
                        GMT -5
                    </option>
                    <option value="America/Bogota" data-icon="gd-ico-global">Bogota, Jamaica, Lima,
                        Panama, Quito, GMT -5
                    </option>
                    <option value="America/Mexico_City" data-icon="gd-ico-global">Mexico City, GMT
                        -5
                    </option>
                    <option value="America/Guatemala" data-icon="gd-ico-global">Belize, Costa Rica,
                        El Salvadore, Guatemala, GMT -6
                    </option>
                    <option value="America/St_Johns" data-icon="gd-ico-global">Newfoundland, GMT
                        -2:30
                    </option>
                    <option value="Asia/Srednekolymsk" data-icon="gd-ico-global">Chersky,
                        Severo-Kurilsk, GMT +11
                    </option>
                    <option value="Asia/Vladivostok" data-icon="gd-ico-global">Deputatsky,
                        Khabarovsk, Oymyakom, Vladivostok, GMT +10
                    </option>
                    <option value="Asia/Magadan" data-icon="gd-ico-global">Guam, Magadan, Solomon
                        Islands, New Caledonia, GMT +11
                    </option>
                    <option value="Asia/Yakutsk" data-icon="gd-ico-global">Olenyok, Vilyuysk,
                        Yakutsk, GMT +9
                    </option>
                    <option value="Asia/Tokyo" data-icon="gd-ico-global">Seoul, Osaka, Tokyo, GMT
                        +9
                    </option>
                    <option value="Asia/Pyongyang" data-icon="gd-ico-global">Pyongyang, GMT +8:30
                    </option>
                    <option value="Asia/Singapore" data-icon="gd-ico-global">Taipei, Kuala Lumpur,
                        Singapore, GMT +8
                    </option>
                    <option value="Asia/Irkutsk" data-icon="gd-ico-global">Bratsk, Chita, Irkutsk,
                        Ulaan Bataar, GMT +8
                    </option>
                    <option value="Asia/Hong_Kong" data-icon="gd-ico-global">Beijing, Chongqing,
                        Hong Kong, Urumqi, GMT +8
                    </option>
                    <option value="Asia/Krasnoyarsk" data-icon="gd-ico-global">Krasnoyarsk, Dudinka,
                        Igarka, Tura, GMT +7
                    </option>
                    <option value="Asia/Bangkok" data-icon="gd-ico-global">Bangkok, Hanoi, Jakarta,
                        GMT +7
                    </option>
                    <option value="Asia/Rangoon" data-icon="gd-ico-global">Rangoon, GMT +6:30
                    </option>
                    <option value="Asia/Almaty" data-icon="gd-ico-global">Astana, Dhaka,
                        Novosibirsk, GMT +6
                    </option>
                    <option value="Asia/Katmandu" data-icon="gd-ico-global">Kathmandu, GMT +5:30
                    </option>
                    <option value="Asia/Calcutta" data-icon="gd-ico-global">Calcutta, Chennai,
                        Mumbai, New Delhi, GMT +5:30
                    </option>
                    <option value="Asia/Yekaterinburg" data-icon="gd-ico-global">Islamabad,
                        Tashkent, Yekaterinburg, GMT +5
                    </option>
                    <option value="Asia/Kabul" data-icon="gd-ico-global">Kabul, GMT +4:30</option>
                    <option value="Asia/Baku" data-icon="gd-ico-global">Baku, GMT +4</option>
                    <option value="Asia/Tbilisi" data-icon="gd-ico-global">Samara, Tbilisi, Yerevan,
                        GMT +4
                    </option>
                    <option value="Asia/Dubai" data-icon="gd-ico-global">Abu Dhabi, Muscat, GMT +4
                    </option>
                    <option value="Asia/Tehran" data-icon="gd-ico-global">Tehran, GMT +3:30</option>
                    <option value="Asia/Baghdad" data-icon="gd-ico-global">Baghdad, Kuwait, Riyadh,
                        GMT +3
                    </option>
                    <option value="Asia/Jerusalem" data-icon="gd-ico-global">Istanbul, Jerusalem,
                        Minsk, Tel Aviv, GMT +3
                    </option>
                    <option value="Africa/Cairo" data-icon="gd-ico-global">Capetown, Cairo, Harare,
                        Johannesburg, Pretoria, GMT +2
                    </option>
                    <option value="Africa/Blantyre" data-icon="gd-ico-global">Kigali, Lusaka,
                        Maputo, Tripoli, GMT +2
                    </option>
                    <option value="Africa/Lagos" data-icon="gd-ico-global">Ndjamena, Kinshasa, West
                        Central Africa, GMT +1
                    </option>
                    <option value="Africa/Monrovia" data-icon="gd-ico-global">Ghana, Monrovia, GMT
                        +0
                    </option>
                    <option value="Atlantic/Cape_Verde" data-icon="gd-ico-global">Praia, Cabo Verde,
                        GMT -1
                    </option>
                    <option value="Atlantic/Azores" data-icon="gd-ico-global">Azores, GMT +0
                    </option>
                    <option value="Pacific/Marquesas" data-icon="gd-ico-global">Taiohae, Marquesas
                        Islands, GMT -9:30
                    </option>
                    <option value="Pacific/Midway" data-icon="gd-ico-global">Midway, Necker Island,
                        Pago Pago, GMT -11
                    </option>
                    <option value="Pacific/Kwajalein" data-icon="gd-ico-global">Kamchatka, Bilbino,
                        Pevek, Marshall Islands, GMT +12
                    </option>
                    <option value="Pacific/Fiji" data-icon="gd-ico-global">Fiji, GMT +12</option>
                    <option value="Pacific/Chatham" data-icon="gd-ico-global">Chatham Islands, GMT
                        +13:30
                    </option>
                    <option value="Pacific/Tongatapu" data-icon="gd-ico-global">Nukualofa, GMT +13
                    </option>
                    <option value="Pacific/Apia" data-icon="gd-ico-global">Apia, GMT +14</option>
                    <option value="Pacific/Kiritimati" data-icon="gd-ico-global">Kiritimati, GMT
                        +14
                    </option>
                    <option value="UTC" data-icon="gd-ico-global">UTC, GMT +0</option>
                </select></div>
        </div>


        <div class="help-block pull-right">
            <a class="js_timezone_toggle" href="javascript:void(0);">Przelicz na czas innej strefy
                czasowej</a>
        </div>
    </div>
    <!-- // WEBINAR DATES -->

    <!-- START sign-up-free-to-register -->
    <div class="details_div">

        <div class="form-group">
            <input type="text" class="form-control" placeholder="Wpisz swoje imię" name="first_name"
                   value="">
            <i class="gd-ico-person-1"></i>
            <span class="asterisk">*</span>
        </div>


        <div class="form-group">
            <input type="email" class="form-control" placeholder="Wpisz swój adres email"
                   name="email_address" value="">
            <i class="gd-ico-mail"></i>
            <span class="asterisk">*</span>
        </div>


    </div>
    <!-- END sign-up-free-to-register -->

    <!-- START GDPR -->
    <div class="gdpr_block js_gdpr_block clearfix" style="">
        <div class="form-group js_gdpr_terms_block gdpr_terms_block">
            <label>
                <input type="checkbox" name="gdpr_terms_mandatory"
                       class="js_gdpr_checkbox js_gdpr_terms_mandatory" value="1">
                <small></small>
                <span class="js_gdpr_label_terms">Tak, zapoznałem się z informacjami o przetwarzaniu danych</span>
            </label>
            <i class="gd-ico-info_circle js_popover_gdpr_trigger"></i>
            <div class="gdpr_terms_offer_popover js_gdpr_terms_offer_popover"
                 style="display: none;">
                <div class="gdpr_terms_offer_popover_wrapper">
                    <div class="popover_content w_nopadding" style="max-height: 300px;">
                                            <span class="js_gdpr_terms_text">
                                                1. POSTANOWIENIA OGÓLNE<br>
                                                1.1. Niniejsza polityka prywatności Sklepu Internetowego ma charakter informacyjny, co oznacza że nie jest ona źródłem obowiązków dla Usługobiorców lub Klientów Sklepu Internetowego. Polityka prywatności zawiera przede wszystkim zasady dotyczące przetwarzania danych osobowych przez Administratora w Sklepie Internetowym, w tym podstawy, cele i zakres przetwarzania danych osobowych oraz prawa osób, których dane dotyczą, a także informacje w zakresie stosowania w Sklepie Internetowym plików cookies oraz narzędzi analitycznych.<br>
<br>
1.2. Administratorem danych osobowych zbieranych za pośrednictwem Sklepu Internetowego jest spółka WIWN.PL SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ SPÓŁKA KOMANDYTOWA z siedzibą w Łodzi wpisana do Krajowego Rejestru Sądowego pod numerem KRS 0000602924, sąd rejestrowy przechowujący dokumentację spółki: Sąd Rejonowy dla Łodzi – Śródmieścia w Łodzi XX Wydział Krajowego Rejestru Sądowego, posiadająca: adres miejsca wykonywania działalności: ul. Piotrkowska 270, 90361 Łódź i adres do doręczeń: ul. Piotrkowska 270/13 p/Go3. ; 90-361 Łódź , NIP: 7252120227, REGON: 363751021, adres poczty elektronicznej: info@wiwn.pl – zwana dalej „Administratorem” i będąca jednocześnie Usługodawcą Sklepu Internetowego i Sprzedawcą.<br>
<br>
1.3. Dane osobowe w Sklepie Internetowym przetwarzane są przez Administratora zgodnie z obowiązującymi przepisami prawa, w szczególności zgodnie z rozporządzeniem Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych) – zwane dalej „RODO” lub „Rozporządzenie RODO”. Oficjalny tekst Rozporządzenia RODO: http://eur-lex.europa.eu/legal-content/PL/TXT/?uri=CELEX%3A32016R0679<br>
<br>
1.4. Korzystanie ze Sklepu Internetowego, w tym dokonywanie zakupów jest dobrowolne. Podobnie związane z tym podanie danych osobowych przez korzystającego ze Sklepu Internetowego Usługobiorcę lub Klienta jest dobrowolne, z zastrzeżeniem dwóch wyjątków: (1) zawieranie umów z Administratorem – niepodanie w przypadkach i w zakresie wskazanym na stronie Sklepu Internetowego oraz w Regulaminie Sklepu Internetowego i niniejszej polityce prywatności danych osobowych niezbędnych do zawarcia i wykonania Umowy Sprzedaży lub umowy o świadczenie Usługi Elektronicznej z Administratorem skutkuje brakiem możliwości zawarcia tejże umowy. Podanie danych osobowych jest w takim wypadku wymogiem umownym i jeżeli osoba, które dane dotyczą chce zawrzeć daną umowę z Administratorem, to jest zobowiązana do podania wymaganych danych. Każdorazowo zakres danych wymaganych do zawarcia umowy wskazany jest uprzednio na stronie Sklepu Internetowego oraz w Regulaminie Sklepu Internetowego; (2) obowiązki ustawowe Administratora – podanie danych osobowych jest wymogiem ustawowym wynikającym z powszechnie obowiązujących przepisów prawa nakładających na Administratora obowiązek przetwarzania danych osobowych (np. przetwarzanie danych w celu prowadzenia ksiąg podatkowych lub rachunkowych) i brak ich podania uniemożliwi Administratorowi wykonanie tychże obowiązków.<br>
<br>
1.5. Administrator dokłada szczególnej staranności w celu ochrony interesów osób, których przetwarzane przez niego dane osobowe dotyczą, a w szczególności jest odpowiedzialny i zapewnia, że zbierane przez niego dane są: (1) przetwarzane zgodnie z prawem;<br>
<br>
(2) zbierane dla oznaczonych, zgodnych z prawem celów i niepoddawane dalszemu przetwarzaniu niezgodnemu z tymi celami; (3) merytorycznie poprawne i adekwatne w stosunku do celów, w jakich są przetwarzane; (4) przechowywane w postaci umożliwiającej identyfikację osób, których dotyczą, nie dłużej niż jest to niezbędne do osiągnięcia celu przetwarzania oraz (5) przetwarzane w sposób zapewniający odpowiednie bezpieczeństwo danych osobowych, w tym ochronę przed niedozwolonym lub niezgodnym z prawem przetwarzaniem oraz przypadkową utratą, zniszczeniem lub uszkodzeniem, za pomocą odpowiednich środków technicznych lub organizacyjnych.<br>
<br>
1.6. Uwzględniając charakter, zakres, kontekst i cele przetwarzania oraz ryzyko naruszenia praw lub wolności osób fizycznych o różnym prawdopodobieństwie i wadze zagrożenia, Administrator wdraża odpowiednie środki techniczne i organizacyjne, aby przetwarzanie odbywało się zgodnie z niniejszym rozporządzeniem i aby móc to wykazać. Środki te są w razie potrzeby poddawane przeglądom i uaktualniane. Administrator stosuje środki techniczne zapobiegające pozyskiwaniu i modyfikowaniu przez osoby nieuprawnione, danych osobowych przesyłanych drogą elektroniczną.<br>
<br>
1.7. Wszelkie słowa, wyrażenia i akronimy występujące w niniejszej polityce prywatności i rozpoczynające się dużą literą (np. Sprzedawca, Sklep Internetowy, Usługa Elektroniczna) należy rozumieć zgodnie z ich definicją zawartą w Regulaminie Sklepu Internetowego dostępnym na stronach Sklepu Internetowego.<br>
<br>
2. PODSTAWY PRZETWARZANIA DANYCH<br>
2.1. Administrator uprawniony jest do przetwarzania danych osobowych w przypadkach, gdy – i w takim zakresie, w jakim – spełniony jest co najmniej jeden z poniższych warunków: (1) osoba, której dane dotyczą wyraziła zgodę na przetwarzanie swoich danych osobowych w jednym lub większej liczbie określonych celów; (2) przetwarzanie jest niezbędne do wykonania umowy, której stroną jest osoba, której dane dotyczą, lub do podjęcia działań na żądanie osoby, której dane dotyczą, przed zawarciem umowy; (3) przetwarzanie jest niezbędne do wypełnienia obowiązku prawnego ciążącego na Administratorze; lub (4) przetwarzanie jest niezbędne do celów wynikających z prawnie uzasadnionych interesów realizowanych przez Administratora lub przez stronę trzecią, z wyjątkiem sytuacji, w których nadrzędny charakter wobec tych interesów mają interesy lub podstawowe prawa i wolności osoby, której dane dotyczą, wymagające ochrony danych osobowych, w szczególności gdy osoba, której dane dotyczą, jest dzieckiem.<br>
<br>
2.2. Przetwarzanie danych osobowych przez Administratora wymaga każdorazowo zaistnienia co najmniej jednej z podstaw wskazanych w pkt. 2.1 polityki prywatności. Konkretne podstawy przetwarzania danych osobowych Usługobiorców i Klientów Sklepu Internetowego przez Administratora są wskazane w kolejnym punkcie polityki prywatności – w odniesieniu do danego celu przetwarzania danych osobowych przez Administratora.<br>
<br>
3. CEL, PODSTAWA, OKRES I ZAKRES PRZETWARZANIA DANYCH W SKLEPIE INTERNETOWYM<br>
3.1. Każdorazowo cel, podstawa, okres i zakres oraz odbiorcy danych osobowych przetwarzanych przez Administratora wynika z działań podejmowanych przez danego Usługobiorcę lub Klienta w Sklepie Internetowym. Przykładowo jeżeli Klient decyduje się na dokonanie zakupów w Sklepie Internetowym i wybierze odbiór osobisty zakupionego Produktu zamiast przesyłki kurierskiej, to jego dane osobowe będą przetwarzane w celu wykonania zawartej Umowy Sprzedaży, ale nie będą już udostępniane przewoźnikowi realizującemu przesyłki na zlecenie Administratora.<br>
<br>
3.2. Administrator może przetwarzać dane osobowe w Sklepie Internetowym w następujących celach, na następujących podstawach, w okresach oraz w następującym zakresie:<br>
<br>
<br>
4. ODBIORCY DANYCH W SKLEPIE INTERNETOWYM<br>
4.1. Dla prawidłowego funkcjonowania Sklepu Internetowego, w tym dla realizacji zawieranych Umów Sprzedaży konieczne jest korzystanie przez Administratora z usług podmiotów zewnętrznych (takich jak np. dostawca oprogramowania, kurier, czy podmiot obsługujący płatności). Administrator korzysta wyłącznie z usług takich podmiotów przetwarzających, którzy zapewniają wystarczające gwarancje wdrożenia odpowiednich środków technicznych i organizacyjnych, tak by przetwarzanie spełniało wymogi Rozporządzenia RODO i chroniło prawa osób, których dane dotyczą.<br>
<br>
4.2. Przekazanie danych przez Administratora nie następuje w każdym wypadku i nie do wszystkich wskazanych w polityce prywatności odbiorców lub kategorii odbiorców – Administrator przekazuje dane wyłącznie wtedy, gdy jest to niezbędne do<br>
<br>
realizacji danego celu przetwarzania danych osobowych i tylko w zakresie niezbędnym do jego zrealizowania. Przykładowo, jeżeli Klient korzysta z odbioru osobistego, to jego dane nie będą przekazywane przewoźnikowi współpracującemu z Administratorem.<br>
<br>
4.3. Dane osobowe Usługobiorców i Klientów Sklepu Internetowego mogą być przekazywane następującym odbiorcom lub kategoriom odbiorców:<br>
<br>
4.3.1. przewoźnicy / spedytorzy / brokerzy kurierscy – w przypadku Klienta, który korzysta w Sklepie Internetowym ze sposobu dostawy Produktu przesyłką pocztową lub przesyłką kurierską, Administrator udostępnia zebrane dane osobowe Klienta wybranemu przewoźnikowi, spedytorowi lub pośrednikowi realizującemu przesyłki na zlecenie Administratora w zakresie niezbędnym do zrealizowania dostawy Produktu Klientowi.<br>
<br>
4.3.2. podmioty obsługujące płatności elektroniczne lub kartą płatniczą – w przypadku Klienta, który korzysta w Sklepie Internetowym ze sposobu płatności elektronicznych lub kartą płatniczą Administrator udostępnia zebrane dane osobowe Klienta wybranemu podmiotowi obsługującemu powyższe płatności w Sklepie Internetowym na zlecenie Administratora w zakresie niezbędnym do obsługi płatności realizowanej przez Klienta.<br>
<br>
4.3.3. dostawcy usług zaopatrujący Administratora w rozwiązania techniczne, informatyczne oraz organizacyjne, umożliwiające Administratorowi prowadzenie działalności gospodarczej, w tym Sklepu Internetowego i świadczonych za jego pośrednictwem Usług Elektronicznych (w szczególności dostawcy oprogramowania komputerowego do prowadzenia Sklepu Internetowego, dostawcy poczty elektronicznej i hostingu oraz dostawcy oprogramowania do zarządzania firmą i udzielania pomocy technicznej Administratorowi) – Administrator udostępnia zebrane dane osobowe Klienta wybranemu dostawcy działającemu na jego zlecenie jedynie w przypadku oraz w zakresie niezbędnym do zrealizowania danego celu przetwarzania danych zgodnego z niniejszą polityką prywatności.<br>
<br>
4.3.4. dostawcy usług księgowych, prawnych i doradczych zapewniający Administratorowi wsparcie księgowe, prawne lub doradcze (w szczególności biuro księgowe, kancelaria prawna lub firma windykacyjna) – Administrator udostępnia zebrane dane osobowe Klienta wybranemu dostawcy działającemu na jego zlecenie jedynie w przypadku oraz w zakresie niezbędnym do zrealizowania danego celu przetwarzania danych zgodnego z niniejszą polityką prywatności.<br>
<br>
5. PROFILOWANIE W SKLEPIE INTERNETOWYM<br>
5.1. Rozporządzenie RODO nakłada na Administratora obowiązek informowania o zautomatyzowanym podejmowaniu decyzji, w tym o profilowaniu, o którym mowa w art. 22 ust. 1 i 4 Rozporządzenia RODO, oraz – przynajmniej w tych przypadkach – istotne informacje o zasadach ich podejmowania, a także o znaczeniu i przewidywanych konsekwencjach takiego przetwarzania dla osoby, której dane dotyczą. Mając to na uwadze Administrator podaje w tym punkcie polityki prywatności informacje dotyczące możliwego profilowania.<br>
<br>
5.2. Administrator może korzystać w Sklepie Internetowym z profilowania do celów marketingu bezpośredniego, ale decyzje podejmowane na jego podstawie przez Administratora nie dotyczą zawarcia lub odmowy zawarcia Umowy Sprzedaży, czy też możliwości korzystania z Usług Elektronicznych w Sklepie Internetowym. Efektem korzystania z profilowania w Sklepie Internetowym może być np. przyznanie danej osobie rabatu, przesłanie jej kodu rabatowego, przypomnienie o niedokończonych zakupach, przesłanie propozycji Produktu, który może odpowiadać zainteresowaniom lub preferencjom danej osoby lub też zaproponowanie lepszych warunków w porównaniu do standardowej oferty Sklepu Internetowego. Mimo profilowania to dana osoba podejmuje swobodnie decyzję, czy będzie chciała skorzystać z otrzymanego w ten sposób rabatu, czy też lepszych warunków i dokonać zakupu w Sklepie Internetowym.<br>
<br>
5.3. Profilowanie w Sklepie Internetowym polega na automatycznej analizie lub prognozie zachowania danej osoby na stronie Sklepu Internetowego np. poprzez dodanie konkretnego Produktu do koszyka, przeglądanie strony konkretnego Produktu w Sklepie Internetowym, czy też poprzez analizę dotychczasowej historii dokonanych zakupów w Sklepie Internetowym. Warunkiem takiego profilowania jest posiadanie przez Administratora danych osobowych danej osoby, aby móc jej następnie przesłać np. kod rabatowy.<br>
<br>
5.4. Osoba, której dane dotyczą, ma prawo do tego, by nie podlegać decyzji, która opiera się wyłącznie na zautomatyzowanym przetwarzaniu, w tym profilowaniu, i wywołuje wobec tej osoby skutki prawne lub w podobny sposób istotnie na nią wpływa.<br>
<br>
6. PRAWA OSOBY, KTÓREJ DANE DOTYCZĄ<br>
6.1. Prawo dostępu, sprostowania, ograniczenia, usunięcia lub przenoszenia – osoba, której dane dotyczą, ma prawo żądania od Administratora dostępu do swoich danych osobowych, ich sprostowania, usunięcia („prawo do bycia zapomnianym”) lub ograniczenia przetwarzania oraz ma prawo do wniesienia sprzeciwu wobec przetwarzania, a także ma prawo do przenoszenia swoich danych. Szczegółowe warunki wykonywania wskazanych wyżej praw wskazane są w art. 15-21 Rozporządzenia RODO.<br>
<br>
6.2. Prawo do cofnięcia zgody w dowolnym momencie – osoba, której dane przetwarzane są przez Administratora na podstawie wyrażonej zgody (na podstawie art. 6 ust. 1 lit. a) lub art. 9 ust. 2 lit. a) Rozporządzenia RODO), to ma ona prawo do cofnięcia zgody w dowolnym momencie bez wpływu na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej cofnięciem.<br>
<br>
6.3. Prawo wniesienia skargi do organu nadzorczego – osoba, której dane przetwarzane są przez Administratora, ma prawo wniesienia skargi do organu nadzorczego w sposób i trybie określonym w przepisach Rozporządzenia RODO oraz prawa polskiego, w szczególności ustawy o ochronie danych osobowych. Organem nadzorczym w Polsce jest Prezes Urzędu Ochrony Danych Osobowych.<br>
<br>
6.4. Prawo do sprzeciwu – osoba, której dane dotyczą, ma prawo w dowolnym momencie wnieść sprzeciw – z przyczyn związanych z jej szczególną sytuacją – wobec przetwarzania dotyczących jej danych osobowych opartego na art. 6 ust. 1 lit. e) (interes lub zadania publiczne) lub f) (prawnie uzasadniony interes administratora), w tym profilowania na podstawie tych przepisów. Administratorowi w takim przypadku nie wolno już przetwarzać tych danych osobowych, chyba że wykaże on istnienie ważnych prawnie uzasadnionych podstaw do przetwarzania, nadrzędnych wobec interesów, praw i wolności osoby, której dane dotyczą, lub podstaw do ustalenia, dochodzenia lub obrony roszczeń.<br>
<br>
6.5. Prawo do sprzeciwu dot. marketingu bezpośredniego – jeżeli dane osobowe są przetwarzane na potrzeby marketingu bezpośredniego, osoba, której dane dotyczą, ma prawo w dowolnym momencie wnieść sprzeciw wobec przetwarzania dotyczących jej danych osobowych na potrzeby takiego marketingu, w tym profilowania, w zakresie, w jakim przetwarzanie jest związane z takim marketingiem bezpośrednim.<br>
<br>
6.6. W celu realizacji uprawnień, o których mowa w niniejszym punkcie polityki prywatności można kontaktować się z Administratorem poprzez przesłanie stosownej wiadomości pisemnie lub pocztą elektroniczną na adres Administratora wskazany na wstępie polityki prywatności lub korzystając z formularza kontaktowego dostępnego na stronie Sklepu Internetowego.<br>
<br>
7. COOKIES W SKLEPIE INTERNETOWYM, DANE EKSPLOATACYJNE I ANALITYKA<br>
7.1. Pliki Cookies (ciasteczka) są to niewielkie informacje tekstowe w postaci plików tekstowych, wysyłane przez serwer i zapisywane po stronie osoby odwiedzającej stronę Sklepu Internetowego (np. na dysku twardym komputera, laptopa, czy też na karcie pamięci smartfona – w zależności z jakiego urządzenia korzysta odwiedzający nasz Sklep Internetowy). Szczegółowe informacje dot. plików Cookies, a także historię ich powstania można znaleźć m.in. tutaj: http://pl.wikipedia.org/wiki/Ciasteczko.<br>
<br>
7.2. Administrator może przetwarzać dane zawarte w plikach Cookies podczas korzystania przez odwiedzających ze strony Sklepu Internetowego w następujących celach: 7.2.1. identyfikacji Usługobiorców jako zalogowanych w Sklepie Internetowym i pokazywania, że są zalogowani; 7.2.2. zapamiętywania Produktów dodanych do koszyka w celu złożenia Zamówienia; 7.2.3. zapamiętywania danych z wypełnianych Formularzy Zamówienia, ankiet lub danych logowania do Sklepu Internetowego; 7.2.4. dostosowywania zawartości strony Sklepu Internetowego do indywidualnych preferencji Usługobiorcy (np. dotyczących kolorów, rozmiaru czcionki, układu strony) oraz optymalizacji korzystania ze stron Sklepu Internetowego; 7.2.5. prowadzenia anonimowych statystyk przedstawiających sposób korzystania ze strony Sklepu Internetowego; 7.2.6. remarketingu, to jest badania cech zachowania odwiedzających Sklep Internetowy poprzez anonimową analizę ich działań (np. powtarzające się wizyty na określonych stronach, słowa kluczowe itp.) w celu stworzenia ich profilu i dostarczenia im reklam dopasowanych do ich przewidywanych zainteresowań, także wtedy kiedy odwiedzają oni inne strony internetowe w sieci reklamowej firmy Google Inc. oraz Facebook Ireland Ltd.;<br>
<br>
7.3. Standardowo większość przeglądarek internetowych dostępnych na rynku domyślnie akceptuje zapisywanie plików Cookies. Każdy ma możliwość określenia warunków korzystania z plików Cookies za pomocą ustawień własnej przeglądarki internetowej. Oznacza to, że można np. częściowo ograniczyć (np. czasowo) lub całkowicie wyłączyć możliwość zapisywania plików Cookies – w tym ostatnim wypadku jednak może to mieć wpływ na niektóre funkcjonalności Sklepu Internetowego (przykładowo niemożliwym może okazać się przejście ścieżki Zamówienia poprzez Formularz Zamówienia z uwagi na niezapamiętywanie Produktów w koszyku podczas kolejnych kroków składania Zamówienia).<br>
<br>
7.4. Ustawienia przeglądarki internetowej w zakresie plików Cookies są istotne z punktu widzenia zgody na korzystanie z plików Cookies przez nasz Sklep Internetowy – zgodnie z przepisami taka zgoda może być również wyrażona poprzez ustawienia przeglądarki internetowej. W braku wyrażenia takiej zgody należy odpowiednio zmienić ustawienia przeglądarki internetowej w zakresie plików Cookies.<br>
<br>
7.5. Szczegółowe informacje na temat zmiany ustawień dotyczących plików Cookies oraz ich samodzielnego usuwania w najpopularniejszych przeglądarkach internetowych dostępne są w dziale pomocy przeglądarki internetowej oraz na poniższych stronach (wystarczy kliknąć w dany link):<br>
<br>
w przeglądarce Chrome<br>
w przeglądarce Firefox<br>
w przeglądarce Internet Explorer<br>
w przeglądarce Opera<br>
w przeglądarce Safari<br>
w przeglądarce Microsoft Edge<br>
7.6. Administrator może korzystać w Sklepie Internetowym z usług Google Analytics, Universal Analytics dostarczanych przez firmę Google Inc. (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA. Usługi te pomagają Administratorowi analizować ruch w Sklepie Internetowym. Gromadzone dane przetwarzane są w ramach powyższych usług w sposób zanonimizowany (są to tzw. dane eksploatacyjne, które uniemożliwiają identyfikację osoby) do generowania statystyk pomocnych w administrowaniu Sklepie Internetowym. Dane te mają charakter zbiorczy i anonimowy, tj. nie zawierają cech identyfikujących (danych osobowych) osoby odwiedzające stronę Sklepu Internetowego. Administrator korzystając z powyższych usług w Sklepie Internetowym gromadzi takie dane jak źródła i medium pozyskania odwiedzjących Sklep Internetowy oraz sposób ich zachowania na stronie Sklepu Internetowego, informacje na temat urządzeń i przeglądarek z których odwiedzają stronę, IP oraz domenę, dane geograficzne oraz dane demograficzne (wiek, płeć) i zainteresowania.<br>
<br>
7.7. Możliwe jest zablokowanie w łatwy sposób przez daną osobę udostępniania Google Analytics informacji o jej aktywności na stronie Sklepu Internetowego – w tym celu można zainstalować dodatek do przeglądarki udostępniany przez firmę Google Inc. dostępny tutaj: https://tools.google.com/dlpage/gaoptout?hl=pl.<br>
<br>
7.8. Administrator może korzystać w Sklepie Internetowym z usługi Piksel Facebooka dostarczanej przez firmę Facebook Ireland Limited (4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irlandia). Usługa ta pomaga Administratorowi mierzyć skuteczność reklam oraz dowiadywać się, jakie działania podejmują odwiedzający sklep internetowy, a także wyświetlać tym osobom dopasowane reklamy. Szczegółowe informacje o działaniu Piksela Facebooka możesz znaleźć pod następującym adresem internetowym: https://www.facebook.com/business/help/742478679120153?helpref=page_content.<br>
<br>
7.9. Zarządzanie działaniem Piksela Facebooka jest możliwe poprzez ustawienia reklam w swoim koncie na portalu Facebook.com: https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen.<br>
<br>
8. POSTANOWIENIA KOŃCOWE<br>
                                                8.1. Sklep Internetowy może zawierać odnośniki do innych stron internetowych. Administrator namawia by po przejściu na inne strony, zapoznać się z polityką prywatności tam ustaloną. Niniejsza polityka prywatności dotyczy tylko Sklepu Internetowego Administratora
                                            </span>
                    </div>
                    <a href="javascript:void(0)" class="js_gdpr_popover_close gdpr_popover_close">×</a>
                </div>
            </div>
        </div>
        <div class="form-group js_gdpr_communications_block">
            <label>
                <input type="checkbox"
                       name="gdpr_communications_mandatory"
                       class="js_gdpr_checkbox js_gdpr_communications_mandatory" value="1">
                <small></small>
                <span class="js_gdpr_label_communications">Yes, I would like to receive future communications</span>
            </label>
        </div>
    </div>
    <!-- END GDPR -->


    <!-- BUTTON SECTION -->
    <button id="register_btn" type="submit"
            class="btn btn-blue_new btn-huge btn-block btn_register disabled"
            data-webinar-type="free" disabled="disabled">
                            <span id="original-button">
                                <span class="title">ZAREJESTRUJ SIĘ TERAZ</span>
                                <i class="gd-ico-arrow-right-12"></i>
                            </span>

        <span id="processing" style="display:none;" class="loader_div">
                                    <img src="https://events.genndi.com/assets/modal/img/36.gif">
                            </span>
        <span id="countdown-wrapper" style="display:none;">
                                    <span class="error_title">Błąd</span>
                                    <span class="circle-outline">
                                        <span id="countdown">0</span>
                                    </span>
                                </span>
    </button>
    <small class="message">Twoje dane zostaną przesłane do organizatora webinaru</small>
    <!-- END OF BUTTON SECTION -->

</form>
