<?php

use app\models\{FileType, FileTypeExtension};
use yii\db\Migration;

/**
 * Class m191013_062719_insert_file_type_initial_values
 */
class m191013_062719_insert_file_type_initial_values extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $fileTypes = ['video/mp4', 'video/webm', 'video/ogg', 'application/x-shockwave-flash'];
        foreach ($fileTypes as $fileType) {
            $newType = new FileType([
                'type' => $fileType
            ]);
            if ($newType->save()) {
                switch ($fileType) {
                    case 'video/mp4':
                        $exts = ['.mp4', '.m4v'];
                        break;
                    case 'video/webm':
                        $exts = ['.webm'];
                        break;
                    case 'video/ogg':
                        $exts = ['.ogg', '.ogv'];
                        break;
                    case 'application/x-shockwave-flash':
                        $exts = ['.swf'];
                        break;
                }

                foreach ($exts as $ext) {
                    $newExt = new FileTypeExtension([
                        'type_id' => $newType->id,
                        'name' => $ext
                    ]);
                    $newExt->save();
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191013_073141_insert_file_type_initial_values cannot be reverted.\n";

        return false;
    }
    */
}
