<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%seance_viewers}}`.
 */
class m191013_184139_create_seance_viewers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%seance_viewers}}', [
            'id' => $this->primaryKey()->unique(),
            'registration_id' => $this->integer(11)->unsigned()->notNull(),
            'viewer_token' => $this->string(80)->notNull(),
            'entranced_at' => $this->dateTime()->notNull(),
            'ip' => $this->string(40)->null()->defaultValue(null),
            'user_agent' => $this->string(255)->null()->defaultValue(null),
            'status' => $this->integer(1)->unsigned()->notNull()->defaultValue(1)
        ]);

        $this->addCommentOnColumn('{{%seance_viewers}}', 'registration_id', 'Rejestracja widza na seans');
        $this->addCommentOnColumn('{{%seance_viewers}}', 'viewer_token', 'Token widza do oglądania seansu');
        $this->addCommentOnColumn('{{%seance_viewers}}', 'entranced_at', 'Data i czas wejścia widza na oglądanie seansu');
        $this->addCommentOnColumn('{{%seance_viewers}}', 'ip', 'IP adres widza');
        $this->addCommentOnColumn('{{%seance_viewers}}', 'user_agent', 'USER_AGENT widza');
        $this->addCommentOnColumn('{{%seance_viewers}}', 'status', 'Status widza: ACTIVE, BLOCKED');

        $this->createIndex(
            'idx-seance_registration',
            '{{%seance_viewers}}',
            'registration_id'
        );
        $this->addForeignKey(
            'fk-seance_registration',
            '{{%seance_viewers}}',
            'registration_id',
            '{{%seance_registrations}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-seance_registration', '{{%seance_viewers}}');
        $this->dropIndex('idx-seance_registration', '{{%seance_viewers}}');

        $this->dropTable('{{%seance_viewers}}');
    }
}
