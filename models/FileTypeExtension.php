<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%file_type_extensions}}".
 *
 * @property string $id
 * @property string $type_id ID typu pliku
 * @property string $name Rozszerzenie
 *
 * @property FileType $type
 */
class FileTypeExtension extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%file_type_extensions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id'], 'integer'],
            [['name'], 'string', 'max' => 12],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileType::class, 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'ID typu pliku'),
            'name' => Yii::t('app', 'Rozszerzenie'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(FileType::class, ['id' => 'type_id']);
    }
}
