<?php

use aneeshikmat\yii2\Yii2TimerCountDown\Yii2TimerCountDown as TimerCountDown;

\aneeshikmat\yii2\Yii2TimerCountDown\Yii2TimerCountDownAsset::register($this);

/* @var $this yii\web\View */

$this->title = 'Webinar';
$callBackScript = <<<JS
            alert('Timer Count Down 6 Is finshed!!');
JS;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-5">
                <h2>Countdown</h2>

                <div class="row">
                    <div class="alert alert-success">
                        Using Widget With Default Option
                    </div>
                    <div class="col-xs-12">
                        <div id="time-down-counter"></div>
                        <?= TimerCountDown::widget([
                            'countDownIdSelector' => 'time-down-counter',
                            'countDownDate' => strtotime('+1 minutes') * 1000,// You most * 1000 to convert time to milisecond
                            'countDownResSperator' => ':',
                            'addSpanForResult' => false,
                            'addSpanForEachNum' => false,
                            'countDownOver' => 'Expired',
                            'countDownReturnData' => 'from-days',
                            'templateStyle' => 0,
                            'getTemplateResult' => 0,
                            'callBack' => $callBackScript
                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        Następne webinary
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="wrapper" class="wrapper">
                        <?php \yii\bootstrap4\Modal::begin([
                            'id' => 'webinarjam_Modal',
                            'class' => 'webinarjamModal',
                            'title' => 'Zarejestruj się na to wydarzenie',
                            'toggleButton' => ['label' => 'ZAREJESTRUJ SIĘ TERAZ'],
                        ]);
                        echo $this->render('registration_form');

                        \yii\bootstrap4\Modal::end();
                        ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-7">
                <h2>Plajer</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu
                    fugiat nulla pariatur.</p>

            </div>
        </div>

    </div>
</div>
