<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%films}}".
 *
 * @property string $id
 * @property string $title Nazwa filmu
 * @property string $short_description Krótki opis filmu
 * @property string $description Pełny opis filmu
 * @property string $posterPath Ścieżka do posteru
 * @property int $status
 * @property string $created_by Kim dodany film
 * @property string $created_at Kiedy dodany filmu
 *
 * @property FileSource[] $fileSources
 * @property User $createdBy
 * @property Seance[] $seances
 */
class Film extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%films}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'created_by', 'created_at'], 'required'],
            [['short_description', 'description'], 'string'],
            [['status', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['title', 'posterPath'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Nazwa filmu'),
            'short_description' => Yii::t('app', 'Krótki opis filmu'),
            'description' => Yii::t('app', 'Pełny opis filmu'),
            'posterPath' => Yii::t('app', 'Ścieżka do posteru'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Kim dodany film'),
            'created_at' => Yii::t('app', 'Kiedy dodany filmu'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileSources()
    {
        return $this->hasMany(FileSource::class, ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeances()
    {
        return $this->hasMany(Seance::class, ['film_id' => 'id']);
    }
}
