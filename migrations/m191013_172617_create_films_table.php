<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%films}}`.
 */
class m191013_172617_create_films_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%films}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255)->notNull(),
            'short_description' => $this->text()->null()->defaultValue(null),
            'description' => $this->text()->null()->defaultValue(null),
            'posterPath' => $this->string(255)->null()->defaultValue(null),
            'status' => $this->integer(1)->notNull()->defaultValue(1),
            'created_by' => $this->integer(11)->notNull()->unsigned(),
            'created_at' => $this->dateTime()->notNull()
        ]);

        $this->addCommentOnColumn('{{%films}}', 'title', 'Nazwa filmu');
        $this->addCommentOnColumn('{{%films}}', 'short_description', 'Krótki opis filmu');
        $this->addCommentOnColumn('{{%films}}', 'description', 'Pełny opis filmu');
        $this->addCommentOnColumn('{{%films}}', 'posterPath', 'Ścieżka do posteru');
        $this->addCommentOnColumn('{{%films}}', 'created_by', 'Kim dodany film');
        $this->addCommentOnColumn('{{%films}}', 'created_at', 'Kiedy dodany filmu');

        $this->createIndex(
            'idx-user_admin',
            '{{%films}}',
            'created_by'
        );
        $this->addForeignKey(
            'fk-user_admin',
            '{{%films}}',
            'created_by',
            '{{%users}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_admin', '{{%films}}');
        $this->dropIndex('idx-user_admin', '{{%films}}');
        $this->dropTable('{{%films}}');
    }
}
