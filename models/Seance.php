<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%seances}}".
 *
 * @property string $id
 * @property string $film_id Film zapłanowany do obejrzenia
 * @property string $started_at Data i czas początku seansu
 * @property int $status Status seansu: WAITING_FOR_PLAING, PLAING, FINISHED
 * @property string $creared_by Kim stworzono seans
 * @property string $creared_at Data i czas tworzenia seansu
 *
 * @property SeanceRegistration[] $seanceRegistrations
 * @property Film $film
 * @property User $crearedBy
 */
class Seance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%seances}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['film_id', 'started_at', 'creared_by', 'creared_at'], 'required'],
            [['film_id', 'status', 'creared_by'], 'integer'],
            [['started_at', 'creared_at'], 'safe'],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Film::class, 'targetAttribute' => ['film_id' => 'id']],
            [['creared_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creared_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'film_id' => Yii::t('app', 'Film zapłanowany do obejrzenia'),
            'started_at' => Yii::t('app', 'Data i czas początku seansu'),
            'status' => Yii::t('app', 'Status seansu: WAITING_FOR_PLAING, PLAING, FINISHED'),
            'creared_by' => Yii::t('app', 'Kim stworzono seans'),
            'creared_at' => Yii::t('app', 'Data i czas tworzenia seansu'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeanceRegistrations()
    {
        return $this->hasMany(SeanceRegistration::class, ['seance_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Film::class, ['id' => 'film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrearedBy()
    {
        return $this->hasOne(User::class, ['id' => 'creared_by']);
    }
}
