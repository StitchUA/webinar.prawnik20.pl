<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%file_types}}".
 *
 * @property string $id
 * @property string $type Typ pliku
 *
 * @property FileSource[] $fileSources
 * @property FileTypeExtesion[] $fileTypeExtesions
 */
class FileType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%file_types}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Typ pliku'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileSources()
    {
        return $this->hasMany(FileSource::class, ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileTypeExtesions()
    {
        return $this->hasMany(FileTypeExtesion::class, ['type_id' => 'id']);
    }
}
