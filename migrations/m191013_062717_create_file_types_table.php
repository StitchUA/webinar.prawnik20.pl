<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%file_types}}`.
 */
class m191013_062717_create_file_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%file_types}}', [
            'id' => $this->primaryKey()->unsigned(),
            'type' => $this->string(50)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%file_types}}');
    }
}
